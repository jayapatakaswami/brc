+++
chapter = true
date = 2020-10-02T23:18:44Z
pre = "<b>1. </b>"
title = "Gaudiya"
weight = 5

+++
### Srila Bhaktisiddhanta Saraswati's Press

# Gaudiya

A short description goes here.

 1. [Front Cover]({{< ref "1" >}})
 2. [Front Matter]({{< ref "2" >}})
 3. [Gauḍīya prīti]({{< ref "7" >}})
 4. [Bhāratīya]({{< ref "8" >}})
 5. [mānasika gaṇanāya adhikāra]({{< ref "8" >}})
 6. [guru kā vāgera avasthā]({{< ref "8" >}})
 7. [jalapā-i guḍite pikeṭīṁ]({{< ref "8" >}})
 8. [mṛtyuḥ]({{< ref "8" >}})
 9. [prabandhe pāritoṣika]({{< ref "9" >}})
10. [mānahāni]({{< ref "9" >}})
11. [telinīpāḍāya dāṅgā]({{< ref "9" >}})
12. [rāja niryyāṇa]({{< ref "9" >}})
13. [khaddara melā]({{< ref "9" >}})
14. [khagera vidāya]({{< ref "9" >}})
15. [hāoḍāya bhikhārī damana]({{< ref "9" >}})
16. [abhinandana sabhā]({{< ref "9" >}})
17. [māra diyā kellā]({{< ref "9" >}})

| Year      | Volume | Article |
| ----------- | ----------- | ----------|
| 2001      | Title       | [Front Cover]({{< ref "1" >}})|
|1923||[Front Matter]({{< ref "2" >}})|
|1923||[Gauḍīya prīti]({{< ref "3" >}})|
|1923||[Bhāratīya]({{< ref "8" >}})|
|1923||[mānasika gaṇanāya adhikāra]({{< ref "8" >}}) |
|1923||[guru kā vāgera avasthā]({{< ref "8" >}}) |
|1923||[jalapā-i guḍite pikeṭīṁ]({{< ref "8" >}}) |
|1923||[mṛtyuḥ]({{< ref "8" >}}) |
|1923||[prabandhe pāritoṣika]({{< ref "9" >}}) |
|1923||[mānahāni]({{< ref "9" >}}) |
|1923||[telinīpāḍāya dāṅgā]({{< ref "9" >}}) |
|1923||[rāja niryyāṇa]({{< ref "9" >}}) |
|1923||[khaddara melā]({{< ref "9" >}}) |
|1923||[khagera vidāya]({{< ref "9" >}}) |
|1923||[hāoḍāya bhikhārī damana]({{< ref "9" >}}) |
|1923||[abhinandana sabhā]({{< ref "9" >}}) |
|1923||[māra diyā kellā]({{< ref "9" >}}) |